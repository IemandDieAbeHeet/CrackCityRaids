package com.abevriens;

import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

public class FactionBorderResizeEvent extends Event implements Cancellable {
    private Faction faction;
    private static final HandlerList HANDLERS_LIST = new HandlerList();
    private boolean isCancelled;

    public FactionBorderResizeEvent(Faction _faction) {
        faction = _faction;
        this.isCancelled = false;
    }

    public Faction getFaction() {
        return faction;
    }

    @Override
    public boolean isCancelled() {
        return isCancelled;
    }

    @Override
    public void setCancelled(boolean c) {
        this.isCancelled = c;
    }

    @NotNull
    @Override
    public HandlerList getHandlers() {
        return HANDLERS_LIST;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS_LIST;
    }
}
