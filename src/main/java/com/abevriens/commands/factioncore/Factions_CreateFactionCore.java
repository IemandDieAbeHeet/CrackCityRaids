package com.abevriens.commands.factioncore;

import com.abevriens.FactionCoreUtil;
import com.abevriens.TextUtil;
import com.abevriens.commands.CommandContext;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;

public class Factions_CreateFactionCore {
    public CommandContext commandContext;

    public Factions_CreateFactionCore(CommandContext _commandContext) {
        commandContext = _commandContext;

        command_CreateFCore();
    }

    private void command_CreateFCore() {
        if(commandContext.cc_player.faction.isEmptyFaction()) {
            ComponentBuilder errorMsg = TextUtil.GenerateNoFactionErrorMsg();
            commandContext.player.spigot().sendMessage(errorMsg.create());
            return;
        } else if(!commandContext.cc_player.faction.factionOwner.uuid.equals(commandContext.cc_player.uuid)) {
            commandContext.player.spigot().sendMessage(TextUtil.GenerateNotOwnerErrorMsg(commandContext.cc_player).create());
            return;
        } else if(!commandContext.cc_player.faction.factionCore.blockLocation.equals(
                new Location(Bukkit.getWorld("world"), 1000000000, 1000000000, 1000000000))) {
            ComponentBuilder errorMessage = TextUtil.GenerateErrorMsg("Je faction heeft al een faction core " +
                    "geplaatst, verplaats je faction core dan met /factions core set.");
            commandContext.player.spigot().sendMessage(errorMessage.create());
            return;
        }

        FactionCoreUtil.PlaceCore(commandContext);
    }
}
