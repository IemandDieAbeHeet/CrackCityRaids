package com.abevriens.commands.teams;

import com.abevriens.CrackCityRaids;
import com.abevriens.Faction;
import com.abevriens.FactionManager;
import com.abevriens.TextUtil;
import com.abevriens.commands.CommandContext;
import net.md_5.bungee.api.chat.ComponentBuilder;

public class Factions_AcceptTeam {
    private final CommandContext commandContext;

    public Factions_AcceptTeam(CommandContext _commandContext) {
        commandContext = _commandContext;

        command_AcceptTeam();
    }

    private void command_AcceptTeam() {
        if(commandContext.cc_player.faction.isEmptyFaction()) {
            commandContext.player.spigot().sendMessage(TextUtil.GenerateNoFactionErrorMsg().create());
        } else if(!commandContext.cc_player.faction.factionOwner.uuid.equals(commandContext.cc_player.uuid)) {
            commandContext.player.spigot().sendMessage(TextUtil.GenerateNotOwnerErrorMsg(commandContext.cc_player).create());
        } else if(commandContext.cc_player.faction.teamRequestFactionName.equals("None")) {
            ComponentBuilder errorMsg = TextUtil.GenerateErrorMsg("Er is geen faction team request naar jouw " +
                    "faction verstuurd!");
            commandContext.player.spigot().sendMessage(errorMsg.create());
        } else {
            Faction teamFaction = CrackCityRaids.factionManager.getFaction(commandContext.cc_player.faction.teamRequestFactionName);
            commandContext.cc_player.faction.teamFactionName = commandContext.cc_player.faction.teamRequestFactionName;
            teamFaction.teamFactionName = commandContext.cc_player.faction.factionName;
            CrackCityRaids.dbHandler.updateFaction(FactionManager.FactionToPOJO(commandContext.cc_player.faction));
            CrackCityRaids.dbHandler.updateFaction(FactionManager.FactionToPOJO(teamFaction));
            ComponentBuilder successMsg = TextUtil.GenerateSuccessMsg("Team request geaccepteerd. Je bent nu geteamd " +
                    "met faction " + commandContext.cc_player.faction.teamRequestFactionName);
            teamFaction.sendMessageToPlayers(successMsg);
            commandContext.cc_player.faction.sendMessageToPlayers(successMsg);
        }
    }
}
