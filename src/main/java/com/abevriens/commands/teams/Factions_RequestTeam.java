package com.abevriens.commands.teams;

import com.abevriens.CrackCityRaids;
import com.abevriens.Faction;
import com.abevriens.FactionManager;
import com.abevriens.TextUtil;
import com.abevriens.commands.CommandContext;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;

public class Factions_RequestTeam {
    private final String askFactionName;
    private final CommandContext commandContext;

    public Factions_RequestTeam(CommandContext _commandContext, String _askFactionName) {
        commandContext = _commandContext;
        askFactionName = _askFactionName;

        command_AskTeam();
    }

    private void command_AskTeam() {
        Faction askFaction = CrackCityRaids.factionManager.getFaction(askFactionName);
        if(askFactionName == null) {
            ComponentBuilder errorMsg = TextUtil.GenerateErrorMsg("Geen faction naam opgegeven, gebruik het commando als " +
                    "volgt:", "/factions team request [naam]");
            commandContext.player.spigot().sendMessage(errorMsg.create());
        } else if (!CrackCityRaids.factionManager.factionNameList.contains(askFactionName)) {
            ComponentBuilder errorMsg = TextUtil.GenerateErrorMsg("De faction die je hebt opgegeven bestaat niet!");
            commandContext.player.spigot().sendMessage(errorMsg.create());
        } else if (commandContext.cc_player.faction.isEmptyFaction()) {
            ComponentBuilder errorMsg = TextUtil.GenerateErrorMsg("Je zit niet in een faction, maak er een aan met " +
                    "/factions create [naam]");
            commandContext.player.spigot().sendMessage(errorMsg.create());
        } else if(!commandContext.cc_player.faction.factionOwner.uuid.equals(commandContext.cc_player.uuid)) {
            ComponentBuilder errorMsg = TextUtil.GenerateNotOwnerErrorMsg(commandContext.cc_player);
            commandContext.player.spigot().sendMessage(errorMsg.create());
        } else if(commandContext.cc_player.faction.factionName.equals(askFactionName)) {
            ComponentBuilder errorMsg = TextUtil.GenerateErrorMsg("Je kunt geen faction team request naar je eigen faction " +
                    "sturen!");
            commandContext.player.spigot().sendMessage(errorMsg.create());
        } else if(!askFaction.teamFactionName.equals("None")) {
            ComponentBuilder errorMsg = TextUtil.GenerateErrorMsg("De faction die je hebt opgegeven heeft al een " +
                    "ander team! Als je toch graag wil teamen moet je aan de opgevraagde faction vragen of ze /factions team " +
                    "cancel doen.");
            commandContext.player.spigot().sendMessage(errorMsg.create());
        } else {
            askFaction.teamRequestFactionName = commandContext.cc_player.faction.factionName;
            CrackCityRaids.dbHandler.updateFaction(FactionManager.FactionToPOJO(commandContext.cc_player.faction));
            ComponentBuilder successMsg = TextUtil.GenerateSuccessMsg("Je hebt faction " + askFactionName + " gevraagd " +
                    "om te teamen!");
            ComponentBuilder askMsg = new ComponentBuilder("Faction " + commandContext.cc_player.faction.factionName + " heeft " +
                    "gevraagd om te teamen. " + askFaction.factionOwner.displayName + " kan dit accepteren met /factions team accept");
            askMsg.color(ChatColor.AQUA);
            askFaction.sendMessageToPlayers(askMsg);
            commandContext.player.spigot().sendMessage(successMsg.create());
        }
    }
}
