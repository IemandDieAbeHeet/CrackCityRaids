package com.abevriens.commands.teams;

import com.abevriens.TextUtil;
import com.abevriens.commands.CommandContext;
import net.md_5.bungee.api.chat.ComponentBuilder;

public class Factions_CancelTeam {
    private final CommandContext commandContext;

    public Factions_CancelTeam(CommandContext _commandContext) {
        commandContext = _commandContext;

        command_CancelTeam();
    }

    private void command_CancelTeam() {
        if(commandContext.cc_player.faction.factionName.equals("None")) {
            commandContext.player.spigot().sendMessage(TextUtil.GenerateNoFactionErrorMsg().create());
        } else if(commandContext.cc_player.faction.teamRequestFactionName.equals("None")) {
            ComponentBuilder errorMsg = TextUtil.GenerateErrorMsg("Je hebt geen faction team request open staan. Vraag " +
                    "een faction om te teamen met", "/factions team request [naam]");
            commandContext.player.spigot().sendMessage(errorMsg.create());
        } else {
            commandContext.cc_player.faction.teamFactionName = "None";
            ComponentBuilder successMsg = TextUtil.GenerateSuccessMsg("Faction team succesvol verwijderd!");
            commandContext.player.spigot().sendMessage(successMsg.create());
        }
    }
}
