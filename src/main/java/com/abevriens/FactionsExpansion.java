package com.abevriens;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.OfflinePlayer;
import org.jetbrains.annotations.NotNull;

public class FactionsExpansion extends PlaceholderExpansion  {
    @Override
    public @NotNull String getIdentifier() {
        return "crackcity";
    }

    @Override
    public @NotNull String getAuthor() {
        return "Abe Vriens";
    }

    @Override
    public @NotNull String getVersion() {
        return "1.0.0";
    }

    @Override
    public boolean persist() {
        return true;
    }

    @Override
    public String onRequest(OfflinePlayer player, String params) {
        if(params.equalsIgnoreCase("faction")){
            CC_Player cc_player = CrackCityRaids.playerManager.getCCPlayer(player);
            return cc_player.faction.factionName;
        }

        return null;
    }
}
