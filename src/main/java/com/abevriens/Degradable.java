package com.abevriens;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

public class Degradable {
    private final String ownerName;

    public int timeLeft;

    public Location degradableBlockLocation;

    Timer timer = new Timer();

    private boolean timerStarted = false;

    public boolean isDegradable() {
        return !FactionCoreUtil.isWithinBounds(
                degradableBlockLocation, CrackCityRaids.factionCoreManager.getClosestFactionCore(degradableBlockLocation));
    }

    public CC_Player getOwner() {
        return CrackCityRaids.playerManager.getCCPlayer(ownerName);
    }

    public Degradable(CC_Player owner, int _timeLeft, Location _degradableBlockLocation) {
        ownerName = owner.displayName;
        timeLeft = _timeLeft;
        degradableBlockLocation = _degradableBlockLocation;
        startDegradeTimer();
    }

    public Block getInventoryBlock() {
        return degradableBlockLocation.getBlock();
    }

    public void startDegradeTimer() {
        if(!isDegradable()) {
            timeLeft = CrackCityRaids.configurationManager.getDegradeTime();
            if(timerStarted) {
                timer.cancel();
                timer = new Timer();
                timerStarted = false;
            }
            return;
        } else if(timerStarted) return;
        timerStarted = true;
        CC_Player cc_player = getOwner();
        OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(UUID.fromString(cc_player.uuid));
        if(offlinePlayer.isOnline()) {
            Player player = offlinePlayer.getPlayer();
            TextComponent text = new TextComponent("Omdat het blok op coördinaten (" + generateLocationString() + ") " +
                    "buiten de faction border valt is deze begonnen met vervallen. Over " +
                    (CrackCityRaids.configurationManager.getDegradeTime()/60) + " uur zal het blok verdwijnen. (Dit bericht wordt " +
                    "voor 60 seconden gedempt)");
            text.setColor(ChatColor.YELLOW);
            if(player != null && cc_player.canSendDegradeMessage()) {
                player.spigot().sendMessage(text);
            }
        }
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                timeLeft--;
                if(timeLeft < 1) {
                    new RemoveDegradableTask(getInventoryBlock()).runTask(CrackCityRaids.instance);
                    if(offlinePlayer.isOnline()) {
                        Player player = offlinePlayer.getPlayer();
                        if(player != null) {
                            TextComponent text = new TextComponent("Blok op coördinaten (" +
                                    generateLocationString() + ") is vervallen!");
                            text.setColor(ChatColor.DARK_RED);
                            player.spigot().sendMessage(text);
                        }
                    }
                    CrackCityRaids.degradableManager.degradableList.remove(Degradable.this);
                    CrackCityRaids.degradableManager.degradableLocationMap.remove(Degradable.this.degradableBlockLocation);
                    CrackCityRaids.dbHandler.deleteDegradable(DegradableManager.degradableToPOJO(Degradable.this));
                    cancel();
                } else {
                    if(offlinePlayer.isOnline()) {
                        Player player = offlinePlayer.getPlayer();
                        if(player != null) {
                            sendDegradeWarning(player);
                        }
                    }
                    CrackCityRaids.dbHandler.updateDegradable(DegradableManager.degradableToPOJO(Degradable.this));
                }
            }
        }, 0, 60 * 1000);
    }

    private String generateLocationString() {
        return "x: " + getInventoryBlock().getLocation().getBlockX() +
                ", y: " + getInventoryBlock().getLocation().getBlockY() +
                ", z: " + getInventoryBlock().getLocation().getBlockZ();
    }

    private void sendDegradeWarning(Player player) {
        TextComponent text = new TextComponent("");
        String locationString = generateLocationString();

        String warningString = "Het blok dat je hebt geplaatst op coördinaten (" + locationString + ") verdwijnt over ";

        if (timeLeft == 60) {
            text.setColor(ChatColor.GREEN);
            text.setText(warningString + "een uur.");
            player.spigot().sendMessage(text);
        } else if (timeLeft == 30) {
            text.setColor(ChatColor.YELLOW);
            text.setText(warningString + "een 30 minuten.");
        } else if (timeLeft == 10) {
            text.setColor(ChatColor.RED);
            text.setText(warningString + "een 10 minuten!");
        } else if (timeLeft == 1) {
            text.setColor(ChatColor.DARK_RED);
            text.setText(warningString + "één minuut!!");
            player.spigot().sendMessage(text);
        }
    }
}
