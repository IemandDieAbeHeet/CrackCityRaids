package com.abevriens.listeners;

import com.abevriens.CC_Player;
import com.abevriens.CrackCityRaids;
import com.abevriens.FactionManager;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class PlayerChatListener implements Listener {
    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        CC_Player cc_player = CrackCityRaids.playerManager.getCCPlayer(event.getPlayer());
        if(!cc_player.faction.factionName.equals(FactionManager.emptyFaction.factionName)) {
            event.setFormat(ChatColor.GREEN + "[" + cc_player.faction.factionName + "] " + event.getFormat());
        }
    }
}
