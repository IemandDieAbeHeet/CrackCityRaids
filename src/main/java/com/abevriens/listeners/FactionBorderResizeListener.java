package com.abevriens.listeners;

import com.abevriens.CrackCityRaids;
import com.abevriens.Degradable;
import com.abevriens.FactionBorderResizeEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class FactionBorderResizeListener implements Listener {
    @EventHandler(priority = EventPriority.LOW)
    public void onFactionBorderResize(FactionBorderResizeEvent event) {
        for(Degradable degradable : CrackCityRaids.degradableManager.degradableList) {
            degradable.startDegradeTimer();
        }
    }
}
