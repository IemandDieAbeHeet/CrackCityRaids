package com.abevriens.listeners;

import com.abevriens.CC_Player;
import com.abevriens.CrackCityRaids;
import com.abevriens.Degradable;
import com.abevriens.DegradableManager;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

import java.util.List;

public class DegradableListener implements Listener {
    List<Material> materialList = CrackCityRaids.configurationManager.getDegradableBlockTypes();

    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockPistonExtend(BlockPistonExtendEvent event) {
        pistonEventCheck(event.getBlocks(), event.getDirection());
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockPistonRetract(BlockPistonRetractEvent event) {
        pistonEventCheck(event.getBlocks(), event.getDirection());
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockExplode(EntityExplodeEvent event) {
        List<Block> blockList = event.blockList();
        for(Block block : blockList) {
            if (!materialList.contains(block.getType())) continue;

            Degradable oldDegradable = CrackCityRaids.degradableManager.degradableLocationMap.get(block.getLocation());
            if(oldDegradable == null) return;
            CrackCityRaids.degradableManager.degradableList.remove(oldDegradable);
            CrackCityRaids.degradableManager.degradableLocationMap.remove(oldDegradable.degradableBlockLocation);
            CrackCityRaids.dbHandler.deleteDegradable(DegradableManager.degradableToPOJO(oldDegradable));
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockBreak(BlockBreakEvent event) {
        if (!materialList.contains(event.getBlock().getType())) return;

        Degradable degradable = CrackCityRaids.degradableManager.degradableLocationMap.get(event.getBlock().getLocation());
        if(degradable == null) return;
        CrackCityRaids.degradableManager.degradableList.remove(degradable);
        CrackCityRaids.degradableManager.degradableLocationMap.remove(degradable.degradableBlockLocation);
        CrackCityRaids.dbHandler.deleteDegradable(DegradableManager.degradableToPOJO(degradable));
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockPlace(BlockPlaceEvent event) {
        if(!materialList.contains(event.getBlockPlaced().getType())) return;

        Player player = event.getPlayer();
        CC_Player cc_player = CrackCityRaids.playerManager.getCCPlayer(player);

        Degradable degradable = new Degradable(cc_player, CrackCityRaids.configurationManager.getDegradeTime(),
                event.getBlockPlaced().getLocation());

        CrackCityRaids.degradableManager.degradableLocationMap.put(event.getBlockPlaced().getLocation(), degradable);
    }

    private void pistonEventCheck(List<Block> blockList, BlockFace direction) {
        for(Block block : blockList) {
            if (!materialList.contains(block.getType())) return;

            switch (block.getPistonMoveReaction()) {
                case MOVE:
                case PUSH_ONLY:
                    Degradable oldDegradable = CrackCityRaids.degradableManager.degradableLocationMap.get(block.getLocation());
                    if(oldDegradable == null) return;
                    CrackCityRaids.degradableManager.degradableList.remove(oldDegradable);
                    CrackCityRaids.degradableManager.degradableLocationMap.remove(oldDegradable.degradableBlockLocation);
                    CrackCityRaids.dbHandler.deleteDegradable(DegradableManager.degradableToPOJO(oldDegradable));

                    CC_Player cc_player = oldDegradable.getOwner();

                    Location newLocation = block.getRelative(direction).getLocation();
                    Degradable newDegradable = new Degradable(cc_player, CrackCityRaids.configurationManager.getDegradeTime(),
                            newLocation);

                    CrackCityRaids.degradableManager.degradableLocationMap.put(newLocation, newDegradable);
                case BREAK:
                    Degradable degradable = CrackCityRaids.degradableManager.degradableLocationMap.get(block.getLocation());
                    if(degradable == null) return;
                    CrackCityRaids.degradableManager.degradableList.remove(degradable);
                    CrackCityRaids.degradableManager.degradableLocationMap.remove(degradable.degradableBlockLocation);
                    CrackCityRaids.dbHandler.deleteDegradable(DegradableManager.degradableToPOJO(degradable));
            }
        }
    }
}
