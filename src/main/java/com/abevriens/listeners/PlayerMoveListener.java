package com.abevriens.listeners;

import com.abevriens.*;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.Objects;

public class PlayerMoveListener implements Listener {
    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        CC_Player cc_player = CrackCityRaids.playerManager.getCCPlayer(player);

        if(Objects.requireNonNull(player.getWorld()).getEnvironment() != World.Environment.NORMAL) return;

        FactionCore closestCore = CrackCityRaids.factionCoreManager.getClosestFactionCore(event.getFrom());

        if(closestCore == null) return;

        Faction closestFaction = CrackCityRaids.factionManager.getFaction(closestCore.factionName);

        if(cc_player.faction == closestFaction ||
                CrackCityRaids.factionManager.getFaction(cc_player.faction.teamFactionName) == closestFaction) {
            if(closestFaction.showTitle) {
                if (FactionCoreUtil.isWithinBounds(event.getFrom(), closestCore)) {
                    if (!cc_player.isWithinFactionBounds) {
                        player.sendTitle(ChatColor.GREEN + closestFaction.factionName,
                                ChatColor.AQUA + "Faction betreden", 5, 40, 10);
                    }
                    cc_player.isWithinFactionBounds = true;
                } else if(!FactionCoreUtil.isWithinBounds(event.getFrom(), closestCore)) {
                    if(cc_player.isWithinFactionBounds) {
                        player.sendTitle(ChatColor.GREEN + closestFaction.factionName,
                                ChatColor.DARK_PURPLE + "Faction verlaten", 5, 40, 10);
                    }
                    cc_player.isWithinFactionBounds = false;
                } else {
                    cc_player.isWithinFactionBounds = false;
                }
            }
            return;
        }

        if(FactionCoreUtil.isWithinBounds(event.getFrom(), closestCore)) {
            ComponentBuilder errorMsg;
            if(closestFaction.raidAlert.openCountdownStarted) {
                ComponentBuilder successMsg = TextUtil.GenerateSuccessMsg("Je bent de faction " +
                        closestFaction.factionName + " betreden. Raid alles binnen " + FactionManager.generateCountdownTimeString(
                                closestFaction.raidAlert.openCountdown));

                if(!cc_player.isWithinFactionBounds) {
                    player.spigot().sendMessage(successMsg.create());
                }

                cc_player.isWithinFactionBounds = true;
            } else if(closestFaction.raidAlert.raidCountdownStarted) {
                errorMsg = TextUtil.GenerateErrorMsg("Je probeert een faction te betreden die niet " +
                        "van jou is. Er is al een raid timer gestart en je kunt over " +
                        FactionManager.generateCountdownTimeString(closestFaction.raidAlert.raidCountdown) +
                        " beginnen met raiden.");

                if(!closestFaction.raidAlert.enteredPlayerList.contains(cc_player.displayName)) {
                    closestFaction.raidAlert.enteredPlayerList.add(cc_player.displayName);
                }
                if(!closestFaction.raidAlert.enteredFactionList.contains(cc_player.faction.factionName)) {
                    closestFaction.raidAlert.enteredFactionList.add(cc_player.faction.factionName);
                }
                
                if (closestFaction.discordIdMap != null) {
                    closestFaction.raidAlert.updateRaidTimerMessage();
                }
                player.spigot().sendMessage(errorMsg.create());
                player.teleport(FactionCoreUtil.getClosestLocationOutsideBounds(player.getLocation(), closestCore));
            } else {
                closestFaction.raidAlert.playersAllowedToConfirm.add(cc_player.uuid);

                ComponentBuilder confirmRaidAlertMsg = new ComponentBuilder();

                TextComponent confirmMsgText = new TextComponent("Je bent binnen de base van faction "
                        + closestFaction.factionName + "!\n Wil je een raid alert versturen? ");
                confirmMsgText.setColor(ChatColor.GOLD);

                TextComponent requestMsgButton = new TextComponent("[Verstuur]");
                requestMsgButton.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/factions confirmalert "
                        + closestFaction.factionName));
                requestMsgButton.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                        new Text("Klik om te versturen!")));

                confirmRaidAlertMsg.append(confirmMsgText).append(requestMsgButton);

                player.spigot().sendMessage(confirmRaidAlertMsg.create());
                player.teleport(FactionCoreUtil.getClosestLocationOutsideBounds(player.getLocation(), closestCore));
            }
        } else {
            cc_player.isWithinFactionBounds = false;
        }

        cc_player.previousLocation = event.getFrom();
    }
}
