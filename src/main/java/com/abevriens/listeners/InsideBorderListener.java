package com.abevriens.listeners;

import com.abevriens.*;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

public class InsideBorderListener implements Listener {
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockBreak(BlockBreakEvent event) {
        Location location = event.getBlock().getLocation();
        if(Objects.requireNonNull(location.getWorld()).getEnvironment() != World.Environment.NORMAL) return;

        Faction closestFaction = CrackCityRaids.factionManager.getFaction(
                CrackCityRaids.factionCoreManager.getClosestFactionCore(location).factionName);

        if(closestFaction.isEmptyFaction()) return;
        if(!FactionCoreUtil.isWithinBounds(location, closestFaction.factionCore)) return;

        Player player = event.getPlayer();
        CC_Player cc_player = CrackCityRaids.playerManager.getCCPlayer(player);

        if(cc_player.faction.factionName.equals(closestFaction.factionName) ||
                cc_player.faction.factionName.equals(closestFaction.teamFactionName)) return;

        if(closestFaction.raidAlert.openCountdownStarted) return;

        ComponentBuilder errorMsg = TextUtil.GenerateErrorMsg("Je kunt dit blok niet breken omdat " +
                "deze binnen een faction staat die niet van jou is. Wanneer er " +
                "een raid is gestart kun je deze wel breken.");
        player.spigot().sendMessage(errorMsg.create());
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityDamage(EntityDamageByEntityEvent event) {
        if(event.getDamager().getType() != EntityType.PLAYER) return;
        Player player = Bukkit.getPlayer(event.getDamager().getUniqueId());
        if(player == null) return;
        if(Objects.requireNonNull(event.getDamager().getLocation().getWorld()).getEnvironment() != World.Environment.NORMAL) return;

        CC_Player cc_player = CrackCityRaids.playerManager.getCCPlayer(player);
        Faction closestFaction = CrackCityRaids.factionManager.getFaction(
                CrackCityRaids.factionCoreManager.getClosestFactionCore(event.getEntity().getLocation()).factionName);
        if(closestFaction.isEmptyFaction()) return;
        if(!FactionCoreUtil.isWithinBounds(event.getEntity().getLocation(), closestFaction.factionCore)) return;
        if(closestFaction.factionName.equals(cc_player.faction.factionName) || closestFaction.factionName.equals(cc_player.faction.teamFactionName))
            return;

        ComponentBuilder errorMsg = TextUtil.GenerateErrorMsg("Je kunt geen damage doen tegen een speler/dier binnen " +
                "iemand anders zijn faction. Wanneer er een raid is gestart kun je dit wel doen.");
        player.spigot().sendMessage(errorMsg.create());
        event.setCancelled(true);
    }

    //TODO: Get the explosion and check if the player causing it is in the faction that the explosion is affects.
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockExplode(EntityExplodeEvent event) {
        Location location = event.getLocation().getBlock().getLocation();

        if(Objects.requireNonNull(location.getWorld()).getEnvironment() != World.Environment.NORMAL) return;
        Faction closestFaction = CrackCityRaids.factionManager.getFaction(
                CrackCityRaids.factionCoreManager.getClosestFactionCore(event.getLocation()).factionName);
        if(closestFaction.isEmptyFaction()) return;
        if(closestFaction.raidAlert.openCountdownStarted) return;
        if(closestFaction.explosionsEnabled) return;
        for(Block explodedBlock : event.blockList()) {
            if(!FactionCoreUtil.isWithinBounds(explodedBlock.getLocation(), closestFaction.factionCore)) continue;

            event.setCancelled(true);
            ComponentBuilder errorMsg = TextUtil.GenerateErrorMsg("(Een deel van) deze blokken kunnen niet opgeblazen worden omdat " +
                    "deze binnen een faction staan waar explosies zijn uitgezet, wanneer er een raid is gestart kun je deze wel kapotmaken.");
            if(location.getWorld() == null) return;
            Collection<Player> players = location.getWorld().getNearbyEntities(location, 50, 100, 50, entity -> entity instanceof Player)
                    .stream()
                    .map(entity -> (Player) entity)
                    .collect(Collectors.toList());

            for(Player player : players) {
                player.spigot().sendMessage(errorMsg.create());
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        CC_Player cc_player = CrackCityRaids.playerManager.getCCPlayer(player);

        if(event.getAction() != Action.RIGHT_CLICK_BLOCK) return;

        Block block = event.getClickedBlock();
        if(block == null) return;
        if(block.getWorld().getEnvironment() != World.Environment.NORMAL) return;

        FactionCore closestCore = CrackCityRaids.factionCoreManager.getClosestFactionCore(block.getLocation());
        Faction closestFaction = CrackCityRaids.factionManager.getFaction(closestCore.factionName);

        if(closestFaction.isEmptyFaction()) return;
        if(closestFaction.factionName.equals(cc_player.faction.factionName)) return;
        if(!closestFaction.teamFactionName.equals(FactionManager.emptyFaction.teamFactionName)) {
            if(closestFaction.teamFactionName.equals(cc_player.faction.factionName)) return;
        }

        if(closestFaction.raidAlert.openCountdownStarted) return;
        if(FactionCoreUtil.isWithinBounds(block.getLocation(), closestCore)) {
            ComponentBuilder errorMsg = TextUtil.GenerateErrorMsg("Je kunt dit blok niet neerzetten/aanraken omdat " +
                    "deze binnen een faction staat die niet van jou is. Wanneer er " +
                    "een raid is gestart kun je deze wel gebruiken.");
            player.spigot().sendMessage(errorMsg.create());
            event.setCancelled(true);
        }
    }
}
