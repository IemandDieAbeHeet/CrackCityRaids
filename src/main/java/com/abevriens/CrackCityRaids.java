package com.abevriens;


import com.abevriens.commands.FactionCommandHandler;
import com.abevriens.commands.FactionCommandTabCompleter;
import com.abevriens.discord.DiscordManager;
import com.abevriens.listeners.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import javax.security.auth.login.LoginException;
import java.util.Objects;

public class CrackCityRaids extends JavaPlugin {
    public static CrackCityRaids instance;
    public static MongoDBHandler dbHandler;
    public static PlayerManager playerManager;
    public static FactionManager factionManager;
    public static FactionCoreManager factionCoreManager;
    public static DiscordManager discordManager;
    public static ConfigurationManager configurationManager;
    public static DegradableManager degradableManager;

    @Override
    public void onEnable() {
        instance = this;
        dbHandler = new MongoDBHandler();
        playerManager = new PlayerManager();
        factionManager = new FactionManager();
        factionCoreManager = new FactionCoreManager();
        configurationManager = new ConfigurationManager();
        degradableManager = new DegradableManager();

        this.getLogger().info(configurationManager.getSpawnConfig().toString());

        try {
            discordManager = new DiscordManager(configurationManager.getDiscordConfig().get("token").toString());
        } catch (LoginException e) {
            this.getLogger().info(ChatColor.RED + "Couldn't connect to the Discord Bot!");
            this.getLogger().info(ChatColor.YELLOW + "Disabling plugin...");
            this.getPluginLoader().disablePlugin(this);
        }

        Objects.requireNonNull(this.getCommand("factions")).setExecutor(new FactionCommandHandler());
        Objects.requireNonNull(this.getCommand("factions")).setTabCompleter(new FactionCommandTabCompleter());
        getServer().getPluginManager().registerEvents(new FactionCoreInteractListener(), this);
        getServer().getPluginManager().registerEvents(new PlayerJoinListener(), this);
        getServer().getPluginManager().registerEvents(new PlayerMoveListener(), this);
        getServer().getPluginManager().registerEvents(new DegradableListener(), this);
        getServer().getPluginManager().registerEvents(new FactionBorderResizeListener(), this);
        getServer().getPluginManager().registerEvents(new InsideBorderListener(), this);
        getServer().getPluginManager().registerEvents(new PlayerChatListener(), this);

        dbHandler.connect("mongodb://localhost:27017/?readPreference=primary&ssl=false");

        if(Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null) {
            new FactionsExpansion().register();
        }
    }

    @Override
    public void onDisable() {
        dbHandler.disconnect();
    }

}