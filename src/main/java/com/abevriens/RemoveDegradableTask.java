package com.abevriens;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

public class RemoveDegradableTask extends BukkitRunnable {
    private final Block containerBlock;

    @Override
    public void run() {
        Chest chest = (Chest) containerBlock.getState();
        ItemStack[] chestContent = chest.getBlockInventory().getContents();
        for (ItemStack item : chestContent) {
            if(item == null) break;
            chest.getWorld().dropItem(chest.getLocation(), item);
        }
        chest.getWorld().dropItem(chest.getLocation(), new ItemStack(chest.getType()));
        containerBlock.setType(Material.AIR);
        cancel();
    }

    public RemoveDegradableTask(Block _inventoryBlock) {
        containerBlock = _inventoryBlock;
    }
}
