package com.abevriens;

import com.abevriens.commands.CommandContext;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.util.Vector;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FactionCoreUtil {
    public static void PlaceCore(CommandContext commandContext) {
        List<Block> facingBlocks = commandContext.player.getLastTwoTargetBlocks(null,  5);

        if(facingBlocks.size() < 2) {
            ComponentBuilder errorMsg = TextUtil.GenerateErrorMsg("Geen blok gevonden om de factions core te plaatsen!");
            commandContext.player.spigot().sendMessage(errorMsg.create());
            return;
        } else if(facingBlocks.get(1).isEmpty()) {
            ComponentBuilder errorMsg = TextUtil.GenerateErrorMsg("Geen blok gevonden om de factions core te plaatsen!");
            commandContext.player.spigot().sendMessage(errorMsg.create());
            return;
        }

        BlockFace face = facingBlocks.get(1).getFace(facingBlocks.get(0));

        if(face == null) {
            return;
        }



        Location check = facingBlocks.get(1).getLocation().add(face.getDirection());

        Block oldBlock = commandContext.cc_player.faction.factionCore.blockLocation.getBlock();

        FactionCore teamFactionCore =
                CrackCityRaids.factionManager.getFaction(commandContext.cc_player.faction.teamFactionName).factionCore;
        FactionCore closestFactionCore =
                CrackCityRaids.factionCoreManager.getClosestFactionCore(check);
        if(!check.getBlock().isEmpty()) {
            ComponentBuilder errorMsg = TextUtil.GenerateErrorMsg("Het blok waar je de factions core probeert te " +
                    "plaatsen is niet leeg!");
            commandContext.player.spigot().sendMessage(errorMsg.create());
        } else if(Objects.requireNonNull(check.getWorld()).getEnvironment() != World.Environment.NORMAL) {
            ComponentBuilder errorMsg = TextUtil.GenerateErrorMsg("Je kunt je faction core alleen in de overworld " +
                    "plaatsen!");
            commandContext.player.spigot().sendMessage(errorMsg.create());
        } else if(closestFactionCore.blockLocation.distance(check) < 200 &&
                !closestFactionCore.factionName.equals(commandContext.cc_player.faction.factionName)) {
            ComponentBuilder errorMsg = TextUtil.GenerateErrorMsg("Je plaatst de faction core te dicht bij een andere " +
                    "faction in de buurt!");
            commandContext.player.spigot().sendMessage(errorMsg.create());
        } else if(teamFactionCore != null && teamFactionCore.blockLocation.distance(check) < 1000) {
            ComponentBuilder errorMsg = TextUtil.GenerateErrorMsg("Je plaatst de faction core te dicht bij de faction core " +
                    "van je team!");
            commandContext.player.spigot().sendMessage(errorMsg.create());
        } else if((Boolean) CrackCityRaids.configurationManager.getSpawnConfig().get("enabled") &&
                ((Vector) CrackCityRaids.configurationManager.getSpawnConfig().get("location")).toLocation(check.getWorld()).distance(check) <
                        ((Integer) CrackCityRaids.configurationManager.getSpawnConfig().get("range"))) {
            ComponentBuilder errorMsg = TextUtil.GenerateErrorMsg("Je plaatst de faction core te dicht bij de spawn, je moet " +
                    "minimaal 500 blokken van de spawn afzitten.");
            commandContext.player.spigot().sendMessage(errorMsg.create());
        } else {
            Block block = check.getBlock();
            Material structureMat = Material.STRUCTURE_BLOCK;
            block.setType(structureMat);
            oldBlock.setType(Material.AIR);
            commandContext.cc_player.faction.factionCore.blockLocation = block.getLocation();
            CrackCityRaids.factionCoreManager.updateFactionCoreLocation(commandContext.cc_player.faction.factionCore);
            CrackCityRaids.dbHandler.updateFaction(
                    FactionManager.FactionToPOJO(commandContext.cc_player.faction));
            BlockData data = block.getBlockData();
            commandContext.player.sendBlockChange(block.getLocation(), data);
            commandContext.cc_player.faction.factionCore.lastChange = Instant.now();
            Bukkit.getPluginManager().callEvent(new FactionBorderResizeEvent(commandContext.cc_player.faction));
        }
    }

    public static void RemoveCore(CommandContext commandContext) {
        FactionCore factionCore = commandContext.cc_player.faction.factionCore;

        if(factionCore == null) {
            ComponentBuilder errorMsg = TextUtil.GenerateErrorMsg("Je faction heeft geen faction core geplaatst!");
            commandContext.player.spigot().sendMessage(errorMsg.create());
            return;
        }

        Block oldBlock = commandContext.cc_player.faction.factionCore.blockLocation.getBlock();

        oldBlock.setType(Material.AIR);
        commandContext.cc_player.faction.factionCore.blockLocation =
                generateEmptyFactionCore(commandContext.cc_player.faction.factionName).blockLocation;
        CrackCityRaids.factionCoreManager.updateFactionCoreLocation(commandContext.cc_player.faction.factionCore);
        CrackCityRaids.dbHandler.updateFaction(
                FactionManager.FactionToPOJO(commandContext.cc_player.faction));
        BlockData data = oldBlock.getBlockData();
        commandContext.player.sendBlockChange(oldBlock.getLocation(), data);
        commandContext.cc_player.faction.factionCore.lastChange = Instant.now();
        Bukkit.getPluginManager().callEvent(new FactionBorderResizeEvent(commandContext.cc_player.faction));
    }

    public static FactionCore generateEmptyFactionCore(String factionName) {
        return new FactionCore(new Location(Bukkit.getWorld("world"), 1000000000, 1000000000, 1000000000), factionName);
    }

    public static boolean isWithinBounds(Location location, FactionCore factionCore) {
        int xLoc = location.getBlockX();
        int yLoc = location.getBlockY();
        int zLoc = location.getBlockZ();

        Faction faction = CrackCityRaids.factionManager.getFaction(factionCore.factionName);
        int blockMinX = factionCore.blockLocation.getBlockX() - faction.xSize/2;
        int blockMaxX = factionCore.blockLocation.getBlockX() + faction.xSize/2;
        int blockMinY = factionCore.blockLocation.getBlockY() - faction.ySize/2;
        int blockMaxY = factionCore.blockLocation.getBlockY() + faction.ySize/2;
        int blockMinZ = factionCore.blockLocation.getBlockZ() - faction.xSize/2;
        int blockMaxZ = factionCore.blockLocation.getBlockZ() + faction.xSize/2;
        return xLoc > blockMinX && xLoc < blockMaxX &&
                yLoc > blockMinY && yLoc < blockMaxY &&
                zLoc > blockMinZ && zLoc < blockMaxZ;
    }

    public static Location getClosestLocationOutsideBounds(Location currentLocation, FactionCore factionCore) {
        Faction faction = CrackCityRaids.factionManager.getFaction(factionCore.factionName);

        int relativeXPos = faction.xSize - (currentLocation.getBlockX() - factionCore.blockLocation.getBlockX());
        int relativeZPos = faction.xSize - (currentLocation.getBlockZ() - factionCore.blockLocation.getBlockZ());
        boolean searchDirection = relativeXPos < relativeZPos;

        //Get the closest border location.
        //TODO: Optionally optimize by first using the faction core location, current location and faction size.
        Location borderLocation = currentLocation;
        for (int xz = 0; xz < faction.xSize / 2; xz++) {
            Block borderSearchBlock;
            if (searchDirection) {
                borderSearchBlock = currentLocation.getBlock().getRelative(xz, 0, 0);
            } else {
                borderSearchBlock = currentLocation.getBlock().getRelative(0, 0, xz);
            }

            if (!isWithinBounds(borderSearchBlock.getLocation(), factionCore)) {
                borderLocation = borderSearchBlock.getLocation();
                borderLocation.setDirection(currentLocation.getDirection());
                break;
            }
        }

        //Check a 20x20 square around border location for valid locations and add them to list.
        List<Location> possibleLocations = new ArrayList<>();
        for (int x = -20; x < 20; x++) {
            for (int y = -borderLocation.getBlockY(); y < (256 - borderLocation.getBlockY()); y++) {
                for (int z = -20; z < 20; z++) {
                    Block searchBlock = borderLocation.getBlock().getRelative(x, y, z);

                    if(!isWithinBounds(searchBlock.getLocation(), factionCore) &&
                            searchBlock.isEmpty() &&
                            !searchBlock.getRelative(BlockFace.DOWN).getRelative(BlockFace.DOWN).isEmpty()) {
                        Location searchLocation = searchBlock.getLocation();
                        if(searchDirection) {
                            searchLocation.add(1, 0, 0);
                        } else {
                            searchLocation.add(0, 0, 1);
                        }
                        possibleLocations.add(searchBlock.getLocation().setDirection(currentLocation.getDirection()));
                    }
                }
            }
        }

        //Get the list of possible locations and get the location closest to the current location.
        Location closestLocation = possibleLocations.get(0);
        for(Location possibleLocation : possibleLocations) {
            if(currentLocation.distance(possibleLocation) < currentLocation.distance(closestLocation)) {
                closestLocation = possibleLocation;
            }
        }

        return closestLocation;
    }
}
