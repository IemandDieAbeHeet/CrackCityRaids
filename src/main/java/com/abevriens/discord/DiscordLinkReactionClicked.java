package com.abevriens.discord;

import com.abevriens.CC_Player;
import com.abevriens.CrackCityRaids;
import com.abevriens.PlayerManager;
import com.abevriens.TextUtil;
import net.dv8tion.jda.api.events.message.priv.react.PrivateMessageReactionAddEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.UUID;

public class DiscordLinkReactionClicked extends ListenerAdapter {
    @Override
    public void onPrivateMessageReactionAdd(@NotNull PrivateMessageReactionAddEvent event) {
        if(event.getUser() == null || event.getUser().isBot()) return;
        if(event.getReactionEmote().getEmoji().equals("\uD83D\uDC4D")) {
            CC_Player requestingPlayer = CrackCityRaids.playerManager.getCCPlayer(Bukkit.getOfflinePlayer(
                    UUID.fromString(CrackCityRaids.playerManager.getDiscordRequest(event.getUserId()))));
            requestingPlayer.discordId = event.getUserId();
            CrackCityRaids.dbHandler.updatePlayer(PlayerManager.CCToPOJO(requestingPlayer));
            OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(UUID.fromString(requestingPlayer.uuid));
            CrackCityRaids.playerManager.addPlayer(
                    Objects.requireNonNull(offlinePlayer),
                    PlayerManager.CCToPOJO(requestingPlayer), requestingPlayer);
            if(offlinePlayer.isOnline()) {
                ComponentBuilder successMsg = TextUtil.GenerateSuccessMsg("Je hebt je Discord account (" +
                        event.getUser().getName() + ") succesvol gelinkt aan je Minecraft account!");
                Player player = offlinePlayer.getPlayer();
                if(player != null) {
                    player.spigot().sendMessage(successMsg.create());
                }
            }
            String messageId = event.getMessageId();
            event.getChannel().deleteMessageById(messageId).queue();
        } else if(event.getReactionEmote().getEmoji().equals("\uD83D\uDC4E")) {
            String messageId = event.getMessageId();
            event.getChannel().deleteMessageById(messageId).queue();
        }
    }
}
