package com.abevriens;

import org.bson.codecs.configuration.CodecConfigurationException;
import org.bukkit.ChatColor;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DegradableManager {
    public List<Degradable> degradableList = new ArrayList<>();
    public Map<Location, Degradable> degradableLocationMap = new HashMap<>();

    public void loadDegradables() {
        try {
            Iterable<POJO_Degradable> degradables = CrackCityRaids.dbHandler.degradableCollection.find();

            for (POJO_Degradable pojo_degradable : degradables) {
                Degradable degradable = pojoToDegradable(pojo_degradable);
                if(!CrackCityRaids.configurationManager.getDegradableBlockTypes().contains(degradable.degradableBlockLocation.getBlock().getType())) {
                    CrackCityRaids.dbHandler.deleteDegradable(pojo_degradable);
                } else {
                    degradableList.add(degradable);
                }
            }
        } catch(CodecConfigurationException e) {
            CrackCityRaids.instance.getLogger().info(e.getMessage());
            CrackCityRaids.instance.getLogger().info(ChatColor.RED + "Couldn't load factions from database, a database entry might be corrupted.");
        }
    }

    public static POJO_Degradable degradableToPOJO(Degradable degradable) {
        POJO_Degradable pojo_degradable =  new POJO_Degradable();
        pojo_degradable.inventoryBlockLocation = new POJO_Vector(degradable.getInventoryBlock().getLocation().toVector());
        pojo_degradable.ownerName = degradable.getOwner().displayName;
        pojo_degradable.timeLeft = degradable.timeLeft;

        return pojo_degradable;
    }

    public static Degradable pojoToDegradable(POJO_Degradable pojo_degradable) {
        return new Degradable(
                CrackCityRaids.playerManager.getCCPlayer(pojo_degradable.ownerName),
                pojo_degradable.timeLeft,
                pojo_degradable.inventoryBlockLocation.pojoVectorToLocation()
        );
    }
}
