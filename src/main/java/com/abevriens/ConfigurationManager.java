package com.abevriens;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class ConfigurationManager {
    public ConfigurationManager() {
        getConfig().options().copyDefaults(true);
        CrackCityRaids.instance.saveDefaultConfig();
    }

    public HashMap<String, Object> getDiscordConfig() {
        return (HashMap<String, Object>) Objects.requireNonNull(getConfig().getConfigurationSection("discord")).getValues(false);
    }

    public HashMap<String, Object> getTeamsConfig() {
        return (HashMap<String, Object>) Objects.requireNonNull(getConfig().getConfigurationSection("teams")).getValues(false);
    }

    public List<Material> getDegradableBlockTypes() {
        List<String> typeNames = getConfig().getStringList("degradables.blocks");
        List<Material> materials = new ArrayList<>();

        for(String name : typeNames) {
            try {
                Material mat = Material.matchMaterial(name);
                materials.add(mat);
            } catch (Exception e) {
                CrackCityRaids.instance.getLogger().info(ChatColor.RED + "Block " + name + " not found, check your spelling.");
            }
        }

        return materials;
    }

    public FileConfiguration getConfig() {
        return CrackCityRaids.instance.getConfig();
    }

    @NotNull
    public HashMap<String, Object> getSpawnConfig() {
        HashMap<String, Object> config =
                (HashMap<String, Object>) Objects.requireNonNull(getConfig().getConfigurationSection("spawn")).getValues(false);

        int x = Integer.parseInt(config.get("x").toString());
        int y = Integer.parseInt(config.get("y").toString());
        int z = Integer.parseInt(config.get("z").toString());

        config.remove("x");
        config.remove("y");
        config.remove("z");
        Vector vector = new Vector(x, y, z);
        config.put("location", vector);
        return config;
    }

    public int getDegradeTime() {
        return getConfig().getInt("degradables.time");
    }
}
